from flask import Flask, request, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://default_u:letmeinmysql@localhost/meetups"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["TEMPLATES_AUTO_RELOAD"] = True
app.config["SECRET_KEY"] = "test"

db = SQLAlchemy(app)
login = LoginManager(app)

from app.routes import *
from app.models import *

if __name__ == "__main__":
	app.run()
