from app.wsgi import db, app
from app.models import *

from flask import request, jsonify, render_template, send_from_directory, redirect, url_for
from flask_login import login_user, current_user, logout_user, login_required
import googlemaps
import json

@app.route("/sd/<fn>")
def sd(fn):
	return send_from_directory("static", fn)

@app.route("/")
def index():
	return render_template("the-landing-page/ridgehack19.html")

@app.route("/login", methods=["GET", "POST"])
def login():
	if request.method == "POST":
		email = request.values.get("email")
		password = request.values.get('password')

		user = User.query.filter_by(email=email).first()

		valid = user.check_password(password)

		if valid:
			print("Login was valid")
			login_user(user)
			return redirect(url_for("index"))
		else:
			return redirect(url_for("login"))


	return render_template("login/login.html")

@app.route('/logout')
def logout():
	logout_user()
	return redirect(url_for("index"))

@app.route("/register", methods=["GET", "POST"])
def register():
	if request.method == "POST":
		first_name = request.values.get('firstName')
		last_name = request.values.get("lastName")
		email = request.values.get('email')
		tags = json.dumps(request.values.get("tags").split(","))
		mentor = request.values.get("mentor")
		password = request.values.get("password")
		bio = request.values.get("bio")

		if mentor == "on":
			mentor = True
		else:
			mentor = False

		new_user = User(name=f"{first_name} {last_name}", email=email, mentor=mentor, bio=bio)
		new_user.set_tags(tags)
		new_user.set_password(password)

		db.session.add(new_user)
		db.session.commit()

		return redirect(url_for('index'))

	return render_template("registration_form/register.html")

@app.route("/my_meetups")
def my_meetups():

	if current_user.mentor:
		meetups = Meetups.query.filter_by(mentor=current_user).all()
	else:
		meetups = Meetups.query.filter_by(student=current_user).all()


	return render_template("mymeetups/my_meetups.html", meetups=meetups)

@app.route("/edit_profile", methods=["GET", "POST"])
def edit_profile():
	if request.method == "POST":
		name = request.values.get("name")
		password = request.values.get("password")
		tags = request.values.get("tags").split(",")
		bio = request.values.get("bio")

		print(tags)

		if name:
			current_user.name = name
		if password:
			current_user.set_password(password)
		if tags:
			current_user.set_tags(tags)
		if bio:
			current_user.bio = bio

		db.session.commit()
		return redirect(url_for("profile"))


	return render_template("profileupdate/profile_update.html", user=current_user)

@app.route("/profile")
def profile():
	return render_template("profilepage/profilepage.html", user=current_user)

@app.route("/find_meetup")
@login_required
def find_meetup():
	return render_template("find_meetup/find_meetup.html", mentors=mentors(current_user))

@app.route("/schedule_meetup/<int:mentor_id>", methods=["GET", "POST"])
def schedule_meetup(mentor_id):
	if request.method == "POST":
		date = request.values.get("date")

		start_time = date + " " + request.values.get("startTime")
		end_time = date + " " + request.values.get("endTime")

		mentor = User.query.filter_by(id=mentor_id).first()

		meetup = Meetups(mentor=mentor, student=current_user, start_time=start_time, end_time=end_time)
		db.session.add(meetup)
		db.session.commit()
		return redirect(url_for("my_meetups"))


		return jsonify({"date": date, "start_time": start_time, "end_time": end_time})

	return render_template("schedulemeetup/schedule.html")

@app.route("/mentor_search")
def mentor_search():

	tag = request.values.get("tag")

	mentors = User.query.filter_by(mentor=1).all()
	mentors_filtered = []
	for mentor in mentors:
		if tag in mentor.get_tags():
			mentors_filtered.append(mentor)

	return render_template("search/search.html", mentors=mentors_filtered)

@app.route("/meetups_logo")
def meetups_logo():
	return send_from_directory("static", "meetups.png")



def mentors(student):
	# based on user id

	mentors = User.query.filter_by(mentor=True).all()
	selected = []

	tags_match = {}
	for mentor in mentors:
		tags = mentor.get_tags()
		user_tags = student.get_tags()

		shared = len(list(set(tags).intersection(user_tags)))
		tags_match[mentor] = shared

	tags_match_fixed = {}


	for mentor, matches in tags_match.items():
		print(matches == 0)
		if matches != 0:
			tags_match_fixed[mentor] = matches

	return tags_match_fixed




