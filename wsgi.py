import sys
import os

sys.path.append(os.getcwd())

from app.wsgi import app

if __name__ == "__main__":
	app.run(host="0.0.0.0", port=5000)
