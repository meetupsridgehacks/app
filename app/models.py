from flask import Flask, request
from app.wsgi import db
from werkzeug.security import check_password_hash


from app.wsgi import app, db, login
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
import json
from flask_login import UserMixin

class User(db.Model, UserMixin):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	mentor = db.Column(db.Boolean)
	name = db.Column(db.String(100))
	bio = db.Column(db.Text(9000))
	email = db.Column(db.String(100))

	tags = db.Column(db.Text(5000))
	location = db.Column(db.Text(5000))
	schedule = db.Column(db.Text(9000))
	password = db.Column(db.Text(256))

	def set_password(self, password):
		self.password = generate_password_hash(password)

	def check_password(self, password):
		return check_password_hash(self.password, password)

	def get_schedule(self):
		return json.loads(self.schedule)

	def get_tags(self):
		tags = json.loads(self.tags)
		if type(tags) == list:
			return tags
		else:
			return json.loads(json.loads(self.tags))


	def set_tags(self, tags):
		self.tags = json.dumps(tags)

	@login.user_loader
	def user_loader(id):
		return User.query.filter_by(id=id).first()



class Meetups(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	mentor_id = db.Column(db.Integer, db.ForeignKey("user.id"))
	mentor = db.relationship("User", backref="mentor_meetups", foreign_keys=[mentor_id])

	student_id = db.Column(db.Integer, db.ForeignKey("user.id"))
	student = db.relationship("User", backref="student_meetups", foreign_keys=[student_id])

	start_time = db.Column(db.DateTime)
	end_time = db.Column(db.DateTime)

class Locations(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	address = db.Column(db.String(5000))

